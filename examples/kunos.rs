use ac_server_plugin_lib::*;
use simplelog::*;

/// Based on the example plugin provided by Kunos.

fn main() -> Result<(), Box<dyn std::error::Error>> {
    TermLogger::init(LevelFilter::Debug, Default::default(), TerminalMode::Stdout)?;

    let cfg = ServerConfig {
        server: PluginPoint {
            command_port: "127.0.0.1:11001".parse()?,
            data_port: "127.0.0.1:12001".parse()?,
        },
        forward: Some(PluginPoint {
            command_port: "127.0.0.1:11101".parse()?,
            data_port: "127.0.0.1:12101".parse()?,
        }),
    };
    let mut server = Server::new(cfg);
    let driver_db = server.get_driver_db();

    server.add_event_handler(DBHandler {
        db: driver_db,
    });
    // server.add_event_handler(ResponseHandler);
    server.add_event_handler(Handler);

    server.run()?;

    Ok(())
}

struct Handler;
impl EventHandler for Handler {
    fn name(&self) -> &str {
        "Test"
    }
    fn on_new_session(&mut self, cmdr: &mut Commander<'_>, info: &SessionInfo) -> Result<(), ServerError> {
        cmdr.set_realtime_interval(std::time::Duration::from_millis(1000))
    }
}

struct DBHandler {
    db: DriverDB,
}

impl EventHandler for DBHandler {
    fn name(&self) -> &str {
        "Driver DB Test Handler"
    }

    fn on_new_connection(&mut self, _: &mut Commander<'_>, _: &ConnectionInfo) -> Result<(), ServerError> {
        let connected_drivers = self.db.get_all_drivers();
        for driver in connected_drivers.iter() {
            println!("{:#?}", driver);
        }

        Ok(())
    }

    fn on_connection_closed(&mut self, _: &mut Commander<'_>, _: &ConnectionInfo) -> Result<(), ServerError> {
        let connected_drivers = self.db.get_all_drivers();
        for driver in connected_drivers.iter() {
            println!("{:#?}", driver);
        }

        Ok(())
    }
}

struct ResponseHandler;

impl EventHandler for ResponseHandler {
    fn name(&self) -> &str {
        "Example Handler"
    }

    fn on_car_info(&mut self, cmdr: &mut Commander<'_>, _: &CarInfo) -> Result<(), ServerError> {
        cmdr.set_session_info(1, "SuperCoolServer", SessionType::Race, SessionLength::Laps(250), 60)?;

        Ok(())
    }

    fn on_chat_message(&mut self, cmdr: &mut Commander<'_>, info: &ChatInfo) -> Result<(), ServerError> {
        match &*info.message {
            "ballast me" => cmdr.set_ballast(info.car_id, 10),
            "restrict me" => cmdr.set_restrictor(info.car_id, 10),
            "kick me" => cmdr.kick_driver(info.car_id),
            "ban me" => cmdr.ban_driver(info.car_id),
            _ => Ok(())
        }
    }

    fn on_client_loaded(&mut self, cmdr: &mut Commander<'_>, car_id: u8) -> Result<(), ServerError> {
        cmdr.send_chat(car_id, "Welcome to the Simple Plugin Example")
    }

    fn on_new_connection(&mut self, cmdr: &mut Commander<'_>, info: &ConnectionInfo) -> Result<(), ServerError> {
        cmdr.get_car_info(info.car_id)
    }

    fn on_new_session(&mut self, cmdr: &mut Commander<'_>, _: &SessionInfo) -> Result<(), ServerError> {
        //cmdr.set_realtime_interval(Duration::from_millis(1000))?;
        cmdr.get_car_info(1)?;
        cmdr.send_chat(0, "CIOA BELLO!")?;
        cmdr.broadcast_message("E' arrivat' 'o 'pirite'")?;

        // Kick user with bad index. Will trigger server error.
        cmdr.kick_driver(230)?;
        Ok(())
    }
}