extern crate failure;
use failure::Error;

extern crate ac_server_plugin;
use ac_server_plugin::{
    Config, ServerPoint, Server, Events,
    ConnectionInfo, Commander,
};

struct PrintHandlerL1;

impl Events for PrintHandlerL1 {
    fn on_new_connection(&mut self, _: &Commander, info: &ConnectionInfo) {
        println!("New Connection");
        println!("Driver: {} - GUID: {}", info.driver_name, info.driver_guid);
        println!("Car: {} - Model: {} - Skin: {}", info.car_id, info.car_model, info.car_skin);
    }

    fn on_client_loaded(&mut self, cmdr: &Commander, id: u8) {
        println!("Client Loaded: {}", id);
        cmdr.send_chat(id, "Welcome from the L1 plugin.");
    }
}

struct PrintHandlerL2;

impl Events for PrintHandlerL2 {
    fn on_new_connection(&mut self, _: &Commander, info: &ConnectionInfo) {
        println!("New Connection");
        println!("Driver: {} - GUID: {}", info.driver_name, info.driver_guid);
        println!("Car: {} - Model: {} - Skin: {}", info.car_id, info.car_model, info.car_skin);
    }

    fn on_client_loaded(&mut self, cmdr: &Commander, id: u8) {
        println!("Client Loaded: {}", id);
        cmdr.send_chat(id, "Welcome from the L2 plugin.");
    }
}

fn run() -> Result<(), Error> {
    let mut args = std::env::args().skip(1);
    let level = args.next()
        .filter(|a| a == "l1" || a == "l2")
        .expect("Requires argument of either \"l1\" or \"l2\"");

    let cfg = if level == "l1" {
        Config {
            server: ServerPoint {
                command_port: "127.0.0.1:11001".parse().unwrap(),
                data_port: "127.0.0.1:12001".parse().unwrap(),
            },
            forward: Some(ServerPoint {
                command_port: "127.0.0.1:11002".parse().unwrap(),
                data_port: "127.0.0.1:12002".parse().unwrap(),
            }),
        }
    } else {
        Config {
            server: ServerPoint {
                command_port: "127.0.0.1:11002".parse().unwrap(),
                data_port: "127.0.0.1:12002".parse().unwrap(),
            },
            forward: None,
        }
    };

    let mut server = Server::new(cfg);
    if level == "l1" {
        server.bind(Box::new(PrintHandlerL1));
    } else {
        server.bind(Box::new(PrintHandlerL2));
    }

    server.run()?;

    Ok(())
}

fn main() {
    use std::process::exit;
    match run() {
        Ok(_) => exit(0),
        Err(e) => {
            eprintln!("Error: {}", e);
            for e in e.iter_causes() {
                eprintln!("Caused by: {}", e);
            }
            exit(1);
        }
    }
}