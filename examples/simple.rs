extern crate failure;
use failure::Error;

extern crate ac_server_plugin;
use ac_server_plugin::{
    Config, ServerPoint, Server, Events,
    LapCompletedInfo, LazyLeaderboard,
    ChatMessage, SessionInfo, ClientEventInfo,
    AcsClientEvent, CarInfo, CarUpdateInfo,
    ConnectionInfo, Commander,
    SessionId, SessionType, SessionLength,
};

struct PrintHandler;

impl Events for PrintHandler {
    fn on_error(&mut self, _: &Commander, error: &str) {
        println!("Server Error: {}", error);
    }

    fn on_chat_message(&mut self, _: &Commander, info: &ChatMessage) {
        println!("Chat from car {}: {}", info.car_id, info.message);
    }

    fn on_client_loaded(&mut self, _: &Commander, car_id: u8) {
        println!("Client Loaded: {}", car_id);
    }

    fn on_protocol_version(&mut self, _: &Commander, version: u8) {
        println!("Protocol Version: {}", version);
    }

    fn on_new_session(&mut self, _: &Commander, info: &SessionInfo) {
        println!("New Session Started");
        self.print_session_info(info);
    }

    fn on_session_info(&mut self, _: &Commander, info: &SessionInfo) {
        self.print_session_info(info);
    }

    fn on_end_session(&mut self, _: &Commander, path: &str) {
        println!("End of Session");
        println!("Report JSON available at {}", path);
    }

    fn on_client_event(&mut self, _: &Commander, info: &ClientEventInfo) {
        match (info.event_type, info.other_car_id) {
            (AcsClientEvent::CollisionWithEnvironment, None) => {
                println!("Collision with Env, Car: {} - Impact Speed: {} - World Pos.: {:?} - Rel. Pos.: {:?}",
                    info.car_id, info.speed, info.world_pos, info.rel_pos
                );
            },
            (AcsClientEvent::CollisionWithCar, Some(other_id)) => {
                println!("Collision with Car, , Car: {} - Other Car: {} - Impact Speed: {} - World Pos.: {:?} - Rel. Pos.: {:?}",
                    info.car_id, other_id, info.speed, info.world_pos, info.rel_pos
                );
            },
            _ => println!("Invalid event combo!")
        }
    }

    fn on_car_info(&mut self, _: &Commander, info: &CarInfo) {
        println!("Car Info");
        println!("Car: {} {} [{}] - Driver: {} - Team: {} - GUID {} - Connected: {}",
                 info.car_id, info.model, info.skin, info.driver_name, info.driver_team,
                 info.driver_guid, info.is_connected,
        );
    }

    fn on_car_update(&mut self, _: &Commander, info: &CarUpdateInfo) {
        println!("Car Update");
        println!("Car: {} - Pos: {:?} - Vel: {:?} - Gear: {} - RPM: {} - NSP: {}",
            info.car_id, info.position, info.velocity, info.gear, info.engine_rpm, info.spline_pos
        );
    }

    fn on_new_connection(&mut self, _: &Commander, info: &ConnectionInfo) {
        println!("New Connection");
        self.print_connection_info(info);
    }

    fn on_connection_closed(&mut self, _: &Commander, info: &ConnectionInfo) {
        println!("Connection Closed");
        self.print_connection_info(info);
    }

    fn on_lap_completed(&mut self, _: &Commander, info: &LapCompletedInfo, leaderboard: &mut LazyLeaderboard) {
        println!("Lap Completed");
        println!("Car {} - Lap: {}ms - Cuts: {}", info.car_id, info.laptime, info.cuts);

        if let Ok(leaderboard) = leaderboard.open() {
            for (pos, entry) in leaderboard.iter().enumerate() {
                println!("{}: Car: {} - Time: {} - Laps: {} - Has Completed: {}",
                    pos+1, entry.car_id, entry.time, entry.laps, entry.has_completed
                );
            }
        }

        println!("Grip Level: {}", info.grip_level);
    }
}

impl PrintHandler {
    fn print_connection_info(&mut self, info: &ConnectionInfo) {
        println!("Driver: {} - GUID: {}", info.driver_name, info.driver_guid);
        println!("Car: {} - Model: {} - Skin: {}", info.car_id, info.car_model, info.car_skin);
    }

    fn print_session_info(&self, info: &SessionInfo) {
        println!("Session Info");
        println!("Protocol Version: {}", info.version);
        println!("Session Index: {}/{} Current Session: {}",
                 info.message_session_index, info.session_count, info.server_session_index);
        println!("Server Name: {}", info.server_name);
        println!("Track: {} [{}]", info.track, info.track_layout);
        println!("Session Name: {}", info.session_name);
        println!("Session Type: {:?}", info.session_type);
        println!("Session Length: {:?}", info.session_length);
        println!("Wait Time: {}", info.wait_time);
        println!("Weather: {}", info.weather);
        println!("Elapsed: {}ms", info.elapsed_ms);
    }
}

struct Responder;

impl Events for Responder {
    fn on_new_session(&mut self, cmdr: &Commander, _: &SessionInfo) {
        cmdr.set_realtime_interval(1000);

        cmdr.get_car_info(1);

        cmdr.send_chat(0, "CIAO BELLO!");
        cmdr.broadcast_message("E' arrivat' 'o 'pirite'");

        // Kick user with bad index, will trigger an error event.
        cmdr.kick_driver(230);
    }

    fn on_client_loaded(&mut self, cmdr: &Commander, car_id: u8) {
        cmdr.send_chat(car_id, "Welcome to the Rust Plugin Simple example.");
    }

    fn on_car_info(&mut self, cmdr: &Commander, info: &CarInfo) {
        cmdr.set_session_info(SessionId::Other(1),
            "SuperCoolServer",
            SessionType::Race,
            SessionLength::Laps(250),
            60
        );
    }

    fn on_new_connection(&mut self, cmdr: &Commander, info: &ConnectionInfo) {
        cmdr.get_car_info(info.car_id);
    }
}

fn run() -> Result<(), Error> {
    let cfg = Config {
        server: ServerPoint {
            command_port: "127.0.0.1:11001".parse().unwrap(),
            data_port: "127.0.0.1:12001".parse().unwrap(),
        },
        forward: None,
    };

    let mut server = Server::new(cfg);
    server.bind(Box::new(PrintHandler));
    server.bind(Box::new(Responder));

    server.run()?;

    Ok(())
}

fn main() {
    use std::process::exit;
    match run() {
        Ok(_) => exit(0),
        Err(e) => {
            eprintln!("Error: {}", e);
            for e in e.iter_causes() {
                eprintln!("Caused by: {}", e);
            }
            exit(1);
        }
    }
}