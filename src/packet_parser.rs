use failure::{Error, err_msg};
use byteorder::{ReadBytesExt, LittleEndian};

use std::io::Cursor;
use std::char::from_u32;
use std::str::from_utf8;
use std::mem::replace;

use protocol::{AcsClientEvent, AcsMessage, SessionLength, SessionType};

#[derive(Debug, Copy, Clone, PartialEq)]
pub struct Vector3f {
    pub x: f32,
    pub y: f32,
    pub z: f32,
}

#[derive(Debug, Eq, PartialEq, Clone)]
pub enum LazyLeaderboard<'s> {
    Raw(&'s [u8]),
    Board(Vec<LeaderboardEntry>),
}

impl<'s> LazyLeaderboard<'s> {
    pub fn open(&mut self) -> Result<&[LeaderboardEntry], Error> {
        if let LazyLeaderboard::Board(b) = self {
            return Ok(b);
        }

        // Only holds a slice at this point.
        let val = self.clone();
        match val {
            LazyLeaderboard::Raw(r) => {
                let mut rdr = Cursor::new(r);
                let len = rdr.read_u8()?;
                let mut leaderboard = Vec::with_capacity(len as usize);

                for _ in 0..len {
                    leaderboard.push(LeaderboardEntry {
                        car_id: rdr.read_u8()?,
                        time: rdr.read_u32::<LittleEndian>()?,
                        laps: rdr.read_u16::<LittleEndian>()?,
                        has_completed: rdr.read_u8()? != 0,
                    })
                }

                replace(self, LazyLeaderboard::Board(leaderboard));
                Ok(self.read().expect("Read of LazyLeaderboard::Board failed."))
            },
            LazyLeaderboard::Board(_) => panic!("Ended up with a board after checking for it."),
        }
    }

    pub fn read(&self) -> Option<&[LeaderboardEntry]> {
        match self {
            LazyLeaderboard::Board(s) => Some(s),
            _ => None,
        }
    }
}

#[derive(Debug, PartialEq, Clone)]
pub struct SessionInfo<'s> {
    pub version: u8,
    pub message_session_index: u8,
    pub server_session_index: u8,
    pub session_count: u8,
    pub server_name: String,
    pub track: &'s str,
    pub track_layout: &'s str,
    pub session_name: &'s str,
    pub session_type: SessionType,
    pub session_length: SessionLength,
    pub wait_time: u16,
    pub ambient_temp: u8,
    pub road_temp: u8,
    pub weather: &'s str,
    pub elapsed_ms: i32,
}

#[derive(Debug, Copy, Clone, PartialEq)]
pub struct ClientEventInfo {
    pub event_type: AcsClientEvent,
    pub car_id: u8,
    pub other_car_id: Option<u8>,
    pub speed: f32,
    pub world_pos: Vector3f,
    pub rel_pos: Vector3f,
}

#[derive(Debug, Clone, Eq, PartialEq)]
pub struct CarInfo {
    pub car_id: u8,
    pub is_connected: bool,
    pub model: String,
    pub skin: String,
    pub driver_name: String,
    pub driver_team: String,
    pub driver_guid: String,
}

#[derive(Debug, Copy, Clone, PartialEq)]
pub struct CarUpdateInfo {
    pub car_id: u8,
    pub position: Vector3f,
    pub velocity: Vector3f,
    pub gear: u8,
    pub engine_rpm: u16,
    pub spline_pos: f32,
}

#[derive(Debug, Clone, Eq, PartialEq)]
pub struct ConnectionInfo<'s> {
    pub driver_name: String,
    pub driver_guid: String,
    pub car_id: u8,
    pub car_model: &'s str,
    pub car_skin: &'s str,
}

#[derive(Debug, Clone, PartialEq)]
pub struct LapCompletedInfo<'s> {
    pub car_id: u8,
    pub laptime: u32,
    pub cuts: u8,
    crate leaderboard: LazyLeaderboard<'s>,
    pub grip_level: f32
}

#[derive(Debug, Clone, Eq, PartialEq)]
pub struct LeaderboardEntry {
    pub car_id: u8,
    pub time: u32,
    pub laps: u16,
    pub has_completed: bool,
}

#[derive(Debug, Clone, Eq, PartialEq)]
pub struct ChatMessage {
    pub car_id: u8,
    pub message: String,
}

#[derive(Debug, PartialEq)]
pub enum RawPacket<'s> {
    ProtocolVersion(u8),
    SessionInfo(SessionInfo<'s>),
    NewSession(SessionInfo<'s>),
    Error(String),
    ClientLoaded(u8),
    ChatMessage(ChatMessage),
    EndSession(String),
    ClientEvent(ClientEventInfo),
    CarInfo(CarInfo),
    CarUpdate(CarUpdateInfo),
    NewConnection(ConnectionInfo<'s>),
    ConnectionClosed(ConnectionInfo<'s>),
    LapCompleted(LapCompletedInfo<'s>),
}


fn read_string_utf32(rdr: &mut Cursor<&[u8]>) -> Result<String, Error> {
    let len = rdr.read_u8()? as usize;
    let mut buf = [0u32; 255];
    let mut buf = &mut buf[..len];
    rdr.read_u32_into::<LittleEndian>(&mut buf)?;

    let string: Result<String, _> = buf.iter()
        .map(|&c| from_u32(c))
        .map(|c| c.ok_or_else(||err_msg("Invalid string data")))
        .collect();

    Ok(string?)
}

fn read_string_ascii<'s>(rdr: &mut Cursor<&[u8]>, data: &'s [u8]) -> Result<&'s str, Error> {
    let len = rdr.read_u8()? as usize;

    let start = rdr.position() as usize;
    rdr.set_position((start + len) as u64);

    Ok(from_utf8(&data[start..start+len])?)
}

fn read_vector3f(rdr: &mut Cursor<&[u8]>) -> Result<Vector3f, Error> {
    Ok(Vector3f {
        x: rdr.read_f32::<LittleEndian>()?,
        y: rdr.read_f32::<LittleEndian>()?,
        z: rdr.read_f32::<LittleEndian>()?,
    })
}



fn parse_client_event(rdr: &mut Cursor<&[u8]>) -> Result<ClientEventInfo, Error> {
    let event_type = AcsClientEvent::from_u8(rdr.read_u8()?).ok_or_else(||err_msg("Unknown client event type"))?;

    Ok(ClientEventInfo{
        event_type,
        car_id: rdr.read_u8()?,
        other_car_id: match event_type {
            AcsClientEvent::CollisionWithCar => Some(rdr.read_u8()?),
            AcsClientEvent::CollisionWithEnvironment => None
        },
        speed: rdr.read_f32::<LittleEndian>()?,
        world_pos: read_vector3f(rdr)?,
        rel_pos: read_vector3f(rdr)?
    })
}

fn parse_car_info(rdr: &mut Cursor<&[u8]>) -> Result<CarInfo, Error> {
    Ok(CarInfo{
        car_id: rdr.read_u8()?,
        is_connected: rdr.read_u8()? != 0,
        model: read_string_utf32(rdr)?,
        skin: read_string_utf32(rdr)?,
        driver_name: read_string_utf32(rdr)?,
        driver_team: read_string_utf32(rdr)?,
        driver_guid: read_string_utf32(rdr)?,
    })
}

fn parse_chat_message(rdr: &mut Cursor<&[u8]>) -> Result<ChatMessage, Error> {
    Ok(ChatMessage{
        car_id: rdr.read_u8()?,
        message: read_string_utf32(rdr)?,
    })
}

fn parse_car_update(rdr: &mut Cursor<&[u8]>) -> Result<CarUpdateInfo, Error> {
    Ok(CarUpdateInfo {
        car_id: rdr.read_u8()?,
        position: read_vector3f(rdr)?,
        velocity: read_vector3f(rdr)?,
        gear: rdr.read_u8()?,
        engine_rpm: rdr.read_u16::<LittleEndian>()?,
        spline_pos: rdr.read_f32::<LittleEndian>()?,
    })
}

fn parse_session_info<'s>(mut rdr: &mut Cursor<&[u8]>, data: &'s [u8]) -> Result<SessionInfo<'s>, Error> {
    Ok(SessionInfo {
        version:                rdr.read_u8()?,
        message_session_index:  rdr.read_u8()?,
        server_session_index:   rdr.read_u8()?,
        session_count:          rdr.read_u8()?,
        server_name:            read_string_utf32(&mut rdr)?,
        track:                  read_string_ascii(&mut rdr, data)?,
        track_layout:           read_string_ascii(&mut rdr, data)?,
        session_name:           read_string_ascii(&mut rdr, data)?,
        session_type:           SessionType::from_u8(rdr.read_u8()?).ok_or_else(|| err_msg("Invalid session type id"))?,
        session_length:         {
            let time = rdr.read_u16::<LittleEndian>()?;
            let laps = rdr.read_u16::<LittleEndian>()?;
            match (time, laps) {
                (0, 0)  => SessionLength::Invalid,
                (0, n)  => SessionLength::Laps(n),
                (n, 0)  => SessionLength::Time(n),
                _       => SessionLength::Invalid,
            }
        },
        wait_time:              rdr.read_u16::<LittleEndian>()?,
        ambient_temp:           rdr.read_u8()?,
        road_temp:              rdr.read_u8()?,
        weather:                read_string_ascii(&mut rdr, data)?,
        elapsed_ms:             rdr.read_i32::<LittleEndian>()?
    })
}

fn parse_connection_change<'s>(rdr: &mut Cursor<&[u8]>, data: &'s [u8]) -> Result<ConnectionInfo<'s>, Error> {
    Ok(ConnectionInfo{
        driver_name: read_string_utf32(rdr)?,
        driver_guid: read_string_utf32(rdr)?,
        car_id: rdr.read_u8()?,
        car_model: read_string_ascii(rdr, data)?,
        car_skin: read_string_ascii(rdr, data)?,
    })
}

fn parse_lap_completed<'s>(rdr: &mut Cursor<&[u8]>, data: &'s [u8]) -> Result<LapCompletedInfo<'s>, Error> {
    Ok(LapCompletedInfo{
        car_id: rdr.read_u8()?,
        laptime: rdr.read_u32::<LittleEndian>()?,
        cuts: rdr.read_u8()?,
        leaderboard: {
            let position = rdr.position() as usize;
            let len = rdr.read_u8()? as usize;
            rdr.set_position((position + len*8) as u64 + 1);

            LazyLeaderboard::Raw(&data[position..position + len*8+1])
        },
        grip_level: rdr.read_f32::<LittleEndian>()?,
    })
}



pub fn parse_packet(data: &[u8]) -> Result<RawPacket, Error> {
    let mut rdr = Cursor::new(data);

    let id = rdr.read_u8()?;
    let id = AcsMessage::from_u8(id).ok_or_else(||err_msg(format!("Unknown packet Id: {}", id)))?;

    let packet = match id {
        AcsMessage::ProtocolVersion    => RawPacket::ProtocolVersion(rdr.read_u8()?),
        AcsMessage::SessionInfo        => RawPacket::SessionInfo(parse_session_info(&mut rdr, data)?),
        AcsMessage::NewSession         => RawPacket::NewSession(parse_session_info(&mut rdr, data)?),
        AcsMessage::Error              => RawPacket::Error(read_string_utf32(&mut rdr)?),
        AcsMessage::ClientLoaded       => RawPacket::ClientLoaded(rdr.read_u8()?),
        AcsMessage::ChatMessage        => RawPacket::ChatMessage(parse_chat_message(&mut rdr)?),
        AcsMessage::EndSession         => RawPacket::EndSession(read_string_utf32(&mut rdr)?),
        AcsMessage::ClientEvent        => RawPacket::ClientEvent(parse_client_event(&mut rdr)?),
        AcsMessage::CarInfo            => RawPacket::CarInfo(parse_car_info(&mut rdr)?),
        AcsMessage::CarUpdate          => RawPacket::CarUpdate(parse_car_update(&mut rdr)?),
        AcsMessage::NewConnection      => RawPacket::NewConnection(parse_connection_change(&mut rdr, data)?),
        AcsMessage::ConnectionClosed   => RawPacket::ConnectionClosed(parse_connection_change(&mut rdr, data)?),
        AcsMessage::LapCompleted       => RawPacket::LapCompleted(parse_lap_completed(&mut rdr, data)?),
    };

    Ok(packet)
}



#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn lap_completed_packet() {
        let data: &[u8]= &[73, 0, 57, 48, 0, 0, 2, 3, 0, 210, 4, 0, 0, 123, 0, 0, 1, 210, 4, 0, 0, 123, 0, 0, 2, 210, 4, 0, 0, 123, 0, 0, 41, 92, 143, 63, 0, 0, 0, 0, 0, 0, 0, 0];
        let mut expected = LapCompletedInfo {
            car_id: 0,
            laptime: 12345,
            cuts: 2,
            leaderboard: LazyLeaderboard::Raw(&data[7..8+8*3]),
            grip_level: 1.12
        };

        let actual = parse_packet(data).unwrap();
        assert_eq!(RawPacket::LapCompleted(expected.clone()), actual);

        let expected_leaderboard = vec![
            LeaderboardEntry {
                car_id: 0,
                time: 1234,
                laps: 123,
                has_completed: false,
            },
            LeaderboardEntry {
                car_id: 1,
                time: 1234,
                laps: 123,
                has_completed: false,
            },
            LeaderboardEntry {
                car_id: 2,
                time: 1234,
                laps: 123,
                has_completed: false,
            }
        ];

        assert_eq!(expected.leaderboard.open().unwrap(), &*expected_leaderboard);
        assert_eq!(expected.leaderboard.open().unwrap(), &*expected_leaderboard);
    }

    #[test]
    fn new_connection_packet() {
        let data: &[u8]= &[51, 9, 116, 0, 0, 0, 101, 0, 0, 0, 115, 0, 0, 0, 116, 0, 0, 0, 32, 0, 0, 0, 110, 0, 0, 0, 97, 0, 0, 0, 109, 0, 0, 0, 101, 0, 0, 0, 9, 116, 0, 0, 0, 101, 0, 0, 0, 115, 0, 0, 0, 116, 0, 0, 0, 32, 0, 0, 0, 103, 0, 0, 0, 117, 0, 0, 0, 105, 0, 0, 0, 100, 0, 0, 0, 0, 8, 116, 101, 115, 116, 32, 99, 97, 114, 9, 116, 101, 115, 116, 32, 115, 107, 105, 110];
        let expected = RawPacket::NewConnection(ConnectionInfo {
            driver_name: "test name".into(),
            driver_guid: "test guid".into(),
            car_id: 0,
            car_model: "test car",
            car_skin: "test skin",
        });

        let actual = parse_packet(data).unwrap();
        assert_eq!(expected, actual);

    }

    #[test]
    fn connection_closed_packet() {
        let data: &[u8]= &[52, 9, 116, 0, 0, 0, 101, 0, 0, 0, 115, 0, 0, 0, 116, 0, 0, 0, 32, 0, 0, 0, 110, 0, 0, 0, 97, 0, 0, 0, 109, 0, 0, 0, 101, 0, 0, 0, 9, 116, 0, 0, 0, 101, 0, 0, 0, 115, 0, 0, 0, 116, 0, 0, 0, 32, 0, 0, 0, 103, 0, 0, 0, 117, 0, 0, 0, 105, 0, 0, 0, 100, 0, 0, 0, 0, 8, 116, 101, 115, 116, 32, 99, 97, 114, 9, 116, 101, 115, 116, 32, 115, 107, 105, 110];
        let expected = RawPacket::ConnectionClosed(ConnectionInfo {
            driver_name: "test name".into(),
            driver_guid: "test guid".into(),
            car_id: 0,
            car_model: "test car",
            car_skin: "test skin",
        });

        let actual = parse_packet(data).unwrap();
        assert_eq!(expected, actual);
    }

    #[test]
    fn version_packet() {
        let data: &[u8]= &[56, 4];
        let parsed = parse_packet(data).unwrap();

        assert_eq!(parsed, RawPacket::ProtocolVersion(4));
    }

    #[test]
    fn new_session_packet() {
        let data: &[u8]= &[50, 4, 0, 0, 1, 10, 78, 0, 0, 0, 101, 0, 0, 0, 119, 0, 0, 0, 32, 0, 0, 0, 83, 0, 0, 0, 101, 0, 0, 0, 114, 0, 0, 0, 118, 0, 0, 0, 101, 0, 0, 0, 114, 0, 0, 0, 15, 107, 115, 95, 98, 114, 97, 110, 100, 115, 95, 104, 97, 116, 99, 104, 4, 105, 110, 100, 121, 8, 80, 114, 97, 99, 116, 105, 99, 101, 1, 44, 1, 0, 0, 0, 0, 18, 25, 7, 51, 95, 99, 108, 101, 97, 114, 0, 0, 0, 0];
        let expected = RawPacket::NewSession(SessionInfo {
            version: 4,
            message_session_index: 0,
            server_session_index: 0,
            session_count: 1,
            server_name: "New Server".into(),
            track: "ks_brands_hatch",
            track_layout: "indy",
            session_name: "Practice",
            session_type: SessionType::Practice,
            session_length: SessionLength::Time(300),
            wait_time: 0,
            ambient_temp: 18,
            road_temp: 25,
            weather: "3_clear",
            elapsed_ms: 0
        });

        let actual = parse_packet(data).unwrap();
        assert_eq!(expected, actual);
    }

    #[test]
    fn session_info_packet() {
        let data: &[u8]= &[59, 4, 0, 0, 1, 10, 78, 0, 0, 0, 101, 0, 0, 0, 119, 0, 0, 0, 32, 0, 0, 0, 83, 0, 0, 0, 101, 0, 0, 0, 114, 0, 0, 0, 118, 0, 0, 0, 101, 0, 0, 0, 114, 0, 0, 0, 15, 107, 115, 95, 98, 114, 97, 110, 100, 115, 95, 104, 97, 116, 99, 104, 4, 105, 110, 100, 121, 8, 80, 114, 97, 99, 116, 105, 99, 101, 1, 44, 1, 0, 0, 0, 0, 18, 25, 7, 51, 95, 99, 108, 101, 97, 114, 0, 0, 0, 0];
        let expected = RawPacket::SessionInfo(SessionInfo {
            version: 4,
            message_session_index: 0,
            server_session_index: 0,
            session_count: 1,
            server_name: "New Server".into(),
            track: "ks_brands_hatch",
            track_layout: "indy",
            session_name: "Practice",
            session_type: SessionType::Practice,
            session_length: SessionLength::Time(300),
            wait_time: 0,
            ambient_temp: 18,
            road_temp: 25,
            weather: "3_clear",
            elapsed_ms: 0
        });

        let actual = parse_packet(data).unwrap();
        assert_eq!(expected, actual);
    }

    #[test]
    fn error_packet() {
        let data: &[u8]= &[60, 10, 84, 0, 0, 0, 101, 0, 0, 0, 115, 0, 0, 0, 116, 0, 0, 0, 32, 0, 0, 0, 69, 0, 0, 0, 114, 0, 0, 0, 114, 0, 0, 0, 111, 0, 0, 0, 114, 0, 0, 0];
        let expected = RawPacket::Error("Test Error".into());

        let actual = parse_packet(data).unwrap();
        assert_eq!(expected, actual);
    }

    #[test]
    fn client_loaded_packet() {
        let data: &[u8]= &[58, 0];
        let expected = RawPacket::ClientLoaded(0);

        let actual = parse_packet(data).unwrap();
        assert_eq!(expected, actual);
    }

    #[test]
    fn chat_message_packet() {
        let data: &[u8]= &[57, 0, 10, 84, 0, 0, 0, 101, 0, 0, 0, 115, 0, 0, 0, 116, 0, 0, 0, 32, 0, 0, 0, 69, 0, 0, 0, 114, 0, 0, 0, 114, 0, 0, 0, 111, 0, 0, 0, 114, 0, 0, 0];
        let expected = RawPacket::ChatMessage(ChatMessage {
            car_id: 0,
            message: "Test Error".into(),
        });

        let actual = parse_packet(data).unwrap();
        assert_eq!(expected, actual);
    }

    #[test]
    fn end_session_packet() {
        let data: &[u8]= &[55, 10, 84, 0, 0, 0, 101, 0, 0, 0, 115, 0, 0, 0, 116, 0, 0, 0, 32, 0, 0, 0, 69, 0, 0, 0, 114, 0, 0, 0, 114, 0, 0, 0, 111, 0, 0, 0, 114, 0, 0, 0];
        let expected = RawPacket::EndSession("Test Error".into());

        let actual = parse_packet(data).unwrap();
        assert_eq!(expected, actual);
    }

    #[test]
    fn client_event_packet() {
        let data: &[u8]= &[130, 10, 0, 1, 41, 92, 143, 63, 20, 174, 7, 64, 20, 174, 71, 64, 10, 215, 131, 64, 10, 215, 163, 64, 10, 215, 195, 64, 10, 215, 227, 64];
        let expected = RawPacket::ClientEvent(ClientEventInfo {
            event_type: AcsClientEvent::CollisionWithCar,
            car_id: 0,
            other_car_id: Some(1),
            speed: 1.12,
            world_pos: Vector3f {
                x: 2.12,
                y: 3.12,
                z: 4.12,
            },
            rel_pos: Vector3f {
                x: 5.12,
                y: 6.12,
                z: 7.12,
            },
        });

        let actual = parse_packet(data).unwrap();
        assert_eq!(expected, actual);
    }

    #[test]
    fn car_info_packet() {
        let data: &[u8]= &[54, 0, 1, 8, 116, 0, 0, 0, 101, 0, 0, 0, 115, 0, 0, 0, 116, 0, 0, 0, 32, 0, 0, 0, 99, 0, 0, 0, 97, 0, 0, 0, 114, 0, 0, 0, 9, 116, 0, 0, 0, 101, 0, 0, 0, 115, 0, 0, 0, 116, 0, 0, 0, 32, 0, 0, 0, 115, 0, 0, 0, 107, 0, 0, 0, 105, 0, 0, 0, 110, 0, 0, 0, 9, 116, 0, 0, 0, 101, 0, 0, 0, 115, 0, 0, 0, 116, 0, 0, 0, 32, 0, 0, 0, 110, 0, 0, 0, 97, 0, 0, 0, 109, 0, 0, 0, 101, 0, 0, 0, 9, 116, 0, 0, 0, 101, 0, 0, 0, 115, 0, 0, 0, 116, 0, 0, 0, 32, 0, 0, 0, 116, 0, 0, 0, 101, 0, 0, 0, 97, 0, 0, 0, 109, 0, 0, 0, 9, 116, 0, 0, 0, 101, 0, 0, 0, 115, 0, 0, 0, 116, 0, 0, 0, 32, 0, 0, 0, 103, 0, 0, 0, 117, 0, 0, 0, 105, 0, 0, 0, 100, 0, 0, 0];
        let expected = RawPacket::CarInfo(CarInfo {
            car_id: 0,
            is_connected: true,
            model: "test car".into(),
            skin: "test skin".into(),
            driver_name: "test name".into(),
            driver_team: "test team".into(),
            driver_guid: "test guid".into(),
        });

        let actual = parse_packet(data).unwrap();
        assert_eq!(expected, actual);
    }

    #[test]
    fn car_update_packet() {
        let data: &[u8]= &[53, 0, 41, 92, 143, 63, 20, 174, 7, 64, 20, 174, 71, 64, 10, 215, 131, 64, 10, 215, 163, 64, 10, 215, 195, 64, 2, 210, 4, 31, 133, 107, 62, 0, 0, 0, 0, 0];
        let expected = RawPacket::CarUpdate(CarUpdateInfo {
            car_id: 0,
            position: Vector3f {
                x: 1.12,
                y: 2.12,
                z: 3.12,
            },
            velocity: Vector3f {
                x: 4.12,
                y: 5.12,
                z: 6.12,
            },
            gear: 2,
            engine_rpm: 1234,
            spline_pos: 0.23,
        });

        let actual = parse_packet(data).unwrap();
        assert_eq!(expected, actual);
    }
}
