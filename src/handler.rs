#![allow(unused_variables)]

use crate::{
    protocol::*,
    commander::Commander,
    error::ServerError,
};

pub trait EventHandler {
    fn name(&self) -> &str;

    fn on_protocol_version(&mut self, cmdr: &mut Commander<'_>, version: u8) -> Result<(), ServerError> {
        Ok(())
    }

    fn on_session_info(&mut self, cmdr: &mut Commander<'_>, info: &SessionInfo) -> Result<(), ServerError> {
        Ok(())
    }

    fn on_new_session(&mut self, cmdr: &mut Commander<'_>, info: &SessionInfo) -> Result<(), ServerError> {
        Ok(())
    }

    fn on_error(&mut self, cmdr: &mut Commander<'_>, msg: &str) -> Result<(), ServerError> {
        Ok(())
    }

    fn on_client_loaded(&mut self, cmdr: &mut Commander<'_>, car_id: u8) -> Result<(), ServerError> {
        Ok(())
    }

    fn on_chat_message(&mut self, cmdr: &mut Commander<'_>, info: &ChatInfo) -> Result<(), ServerError> {
        Ok(())
    }

    fn on_end_session(&mut self, cmdr: &mut Commander<'_>, path: &str) -> Result<(), ServerError> {
        Ok(())
    }

    fn on_client_event(&mut self, cmdr: &mut Commander<'_>, path: &ClientEventInfo) -> Result<(), ServerError> {
        Ok(())
    }

    fn on_car_info(&mut self, cmdr: &mut Commander<'_>, info: &CarInfo) -> Result<(), ServerError> {
        Ok(())
    }

    fn on_car_update(&mut self, cmdr: &mut Commander<'_>, info: &CarUpdateInfo) -> Result<(), ServerError> {
        Ok(())
    }

    fn on_new_connection(&mut self, cmdr: &mut Commander<'_>, info: &ConnectionInfo) -> Result<(), ServerError> {
        Ok(())
    }

    fn on_connection_closed(&mut self, cmdr: &mut Commander<'_>, info: &ConnectionInfo) -> Result<(), ServerError> {
        Ok(())
    }

    fn on_lap_completed(&mut self, cmdr: &mut Commander<'_>, info: &LapCompletedInfo) -> Result<(), ServerError> {
        Ok(())
    }
}
