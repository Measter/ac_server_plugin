use thiserror::Error;

use std::{any::Any, io};

#[derive(Debug, Error)]
pub enum ServerError {
    #[error("Network Error")]
    CommunicationError(#[from] io::Error),

    #[error("Attempted to set an empty name")]
    EmptyNameError,

    #[error("Requested an invalid ballast amount (must be <= 150): {0}")]
    InvalidBallastAmount(u8),

    #[error("Requested an invalid restrictor amount (must be <= 100): {0}")]
    InvalidRestrictorAmount(u8),

    #[error("An unhandled threading error ocurred")]
    UnhandledResult(Box<dyn Any + 'static + Send>),
}
