use crate::{commander::Commander, error::ServerError, handler::EventHandler, protocol::*};

use log::*;

use std::{
    cell::{Ref, RefCell},
    collections::HashMap,
    ops::Deref,
    rc::Rc,
};

#[derive(Debug, Clone, Default)]
pub struct DriverCar {
    pub id: u8,
    pub model: String,
    pub skin: String,
    pub position: Vector3F,
    pub velocity_vector: Vector3F,
    pub speed: f32,
    pub spline_position: f32,
    pub gear: u8,
    pub engine_rpm: u16,
}

#[derive(Debug, Clone)]
pub struct Driver {
    pub name: String,
    pub team: String,
    pub guid: String,
    pub is_connected: bool,
    pub car: Option<DriverCar>,
}

impl Driver {
    pub(crate) fn new(guid: String) -> Self {
        Driver {
            name: String::new(),
            team: String::new(),
            guid,
            is_connected: false,
            car: None,
        }
    }
}

#[derive(Debug, Default)]
pub struct DriverDBInternal {
    drivers: Vec<Driver>,
    drivers_by_guid: HashMap<String, usize>,
    drivers_by_id: HashMap<u8, usize>,
}

#[derive(Debug)]
pub(crate) struct DriverDBHandler {
    pub(crate) db: Rc<RefCell<DriverDBInternal>>,
}

impl EventHandler for DriverDBHandler {
    fn name(&self) -> &str {
        "Driver Database Handler"
    }

    fn on_car_info(&mut self, _: &mut Commander<'_>, info: &CarInfo) -> Result<(), ServerError> {
        if info.driver_guid.is_empty() {
            debug!("Get new connection with empty GUID: {}", info.car_id);
            trace!("Info: {:#?}", info);
            return Ok(());
        }

        let mut db = self.db.borrow_mut();
        let contains_guid = db.drivers_by_guid.contains_key(&info.driver_guid);

        let (driver, index) = if contains_guid {
            let index = db.drivers_by_guid[&info.driver_guid];
            (&mut db.drivers[index], index)
        } else {
            let driver = Driver::new(info.driver_guid.clone());
            db.drivers.push(driver);
            let index = db.drivers.len() - 1;
            db.drivers_by_guid.insert(info.driver_guid.clone(), index);

            (&mut db.drivers[index], index)
        };

        let car_info = driver.car.take();

        driver.name.clear();
        driver.name.push_str(&info.driver_name);
        driver.team.clear();
        driver.team.push_str(&info.driver_team);
        driver.is_connected = info.is_connected;
        driver.car = Some(DriverCar {
            id: info.car_id,
            model: info.car_model.clone(),
            skin: info.car_skin.clone(),
            ..DriverCar::default()
        });

        // Handle the case where we thought the driver was connected with a different
        // car ID.
        match car_info {
            Some(car) if car.id != info.car_id && db.drivers_by_id.contains_key(&car.id) => {
                debug!("Moving driver from ID {} to ID {}", car.id, info.car_id);
                let old_driver_idx = db.drivers_by_id[&car.id];
                let old_driver = &mut db.drivers[old_driver_idx];
                trace!("Old drive info: {:#?}", old_driver);

                if old_driver.guid != info.driver_guid {
                    warn!(
                        "Old driver ({}) is not the same as new drive ({})",
                        old_driver.guid, info.driver_guid,
                    );

                    db.drivers_by_id.remove(&car.id);
                }
            }
            _ => {}
        }

        db.drivers_by_id.insert(info.car_id, index);

        Ok(())
    }

    fn on_protocol_version(&mut self, _: &mut Commander<'_>, _: u8) -> Result<(), ServerError> {
        // The first packet the server sends on startup is the protocol version.
        // If we get this, that means the server has just been started, and we need
        // to mark all drivers as disconnected.

        let mut db = self.db.borrow_mut();
        for driver in &mut db.drivers {
            driver.is_connected = false;
            driver.car = None;
        }

        db.drivers_by_id.clear();

        Ok(())
    }

    fn on_car_update(
        &mut self,
        cmdr: &mut Commander<'_>,
        info: &CarUpdateInfo,
    ) -> Result<(), ServerError> {
        let mut db = self.db.borrow_mut();
        if !db.drivers_by_id.contains_key(&info.car_id) {
            info!("Unknown driver {}, requesting info", info.car_id);
            return cmdr.get_car_info(info.car_id);
        }

        let driver_idx = db.drivers_by_id[&info.car_id];
        let driver = &mut db.drivers[driver_idx];

        if let Some(car_info) = &mut driver.car {
            car_info.speed = info.velocity.length();
            car_info.position = info.position;
            car_info.velocity_vector = info.velocity;
            car_info.gear = info.gear;
            car_info.engine_rpm = info.engine_rpm;

            Ok(())
        } else {
            warn!(
                "Somehow don't have car info for driver {}, requesting info",
                driver.guid
            );
            cmdr.get_car_info(info.car_id)
        }
    }

    fn on_client_loaded(
        &mut self,
        cmdr: &mut Commander<'_>,
        car_id: u8,
    ) -> Result<(), ServerError> {
        let mut db = self.db.borrow_mut();
        if !db.drivers_by_id.contains_key(&car_id) {
            info!("Unknown driver {}, requesting info", car_id);
            cmdr.get_car_info(car_id)
        } else {
            let driver_idx = db.drivers_by_id[&car_id];
            let driver = &mut db.drivers[driver_idx];
            driver.is_connected = true;

            Ok(())
        }
    }

    fn on_connection_closed(
        &mut self,
        _: &mut Commander<'_>,
        info: &ConnectionInfo,
    ) -> Result<(), ServerError> {
        if info.driver_guid.is_empty() {
            debug!("Get new connection with empty GUID: {}", info.car_id);
            trace!("Info: {:#?}", info);
            return Ok(());
        }

        debug!("Driver disconnected ({})", info.driver_guid);

        let mut db = self.db.borrow_mut();
        let driver_idx = if let Some(driver_idx) = db.drivers_by_guid.get(&info.driver_guid) {
            *driver_idx
        } else {
            warn!("Unknown driver disconnected: {}", info.driver_guid);
            trace!("Disconnect info: {:#?}", info);
            return Ok(());
        };

        let driver = &mut db.drivers[driver_idx];
        driver.name.clear();
        driver.name.push_str(&info.driver_name);
        driver.is_connected = false;
        let car_info = driver.car.take();

        if db.drivers_by_id.contains_key(&info.car_id) {
            db.drivers_by_id.remove(&info.car_id);
        } else {
            warn!("Unknown driver ID disconnected: {}", info.car_id);
            trace!("Disconnect info: {:#?}", info);
            return Ok(());
        }

        // Handle possibility that the disconnecting driver had a differant ID in the
        // database than they did in the disconnect packet.
        match car_info {
            Some(car) if car.id != info.car_id && db.drivers_by_id.contains_key(&car.id) => {
                warn!(
                    "Driver had different ID in database ({}) than info packet ({})",
                    car.id, info.car_id,
                );
                let old_driver_idx = db.drivers_by_id[&car.id];
                let old_driver = &mut db.drivers[old_driver_idx];
                trace!("Old driver info: {:#?}", old_driver);

                if old_driver.guid != info.driver_guid {
                    warn!(
                        "Old driver ({}) is not the same as new driver ({})",
                        old_driver.guid, info.driver_guid,
                    );
                    old_driver.car = None;
                }
            }
            _ => {}
        }

        Ok(())
    }

    fn on_new_connection(
        &mut self,
        _: &mut Commander<'_>,
        info: &ConnectionInfo,
    ) -> Result<(), ServerError> {
        if info.driver_guid.is_empty() {
            debug!("Get new connection with empty GUID: {}", info.car_id);
            trace!("Info: {:#?}", info);
            return Ok(());
        }

        debug!("Driver Connected ({})", info.driver_guid);

        let mut db = self.db.borrow_mut();
        let contains_guid = db.drivers_by_guid.contains_key(&info.driver_guid);
        let contains_id = db.drivers_by_id.contains_key(&info.car_id);

        let (driver, index) = if contains_guid {
            let index = db.drivers_by_guid[&info.driver_guid];
            (&mut db.drivers[index], index)
        } else {
            let driver = Driver::new(info.driver_guid.clone());
            db.drivers.push(driver);
            let index = db.drivers.len() - 1;
            db.drivers_by_guid.insert(info.driver_guid.clone(), index);

            (&mut db.drivers[index], index)
        };

        let old_id = driver.car.take();

        driver.name.clear();
        driver.name.push_str(&info.driver_name);
        driver.is_connected = false;
        driver.car = Some(DriverCar {
            id: info.car_id,
            model: info.car_model.clone(),
            skin: info.car_skin.clone(),
            ..DriverCar::default()
        });

        // Handle case where driver is replacing another driver's car.
        if contains_id {
            warn!("Driver with new ID ({}) alread connected!", info.car_id);
            let old_driver_idx = db.drivers_by_id[&info.car_id];
            let old_driver = &mut db.drivers[old_driver_idx];
            trace!("Old driver info: ({:#?})", old_driver);
            old_driver.car = None;
            old_driver.is_connected = false;
        }

        // Handle the case where we thought the driver was connected with a different
        // car ID.
        match old_id {
            Some(car) if car.id != info.car_id && db.drivers_by_id.contains_key(&car.id) => {
                warn!("Driver had old ID ({}) on new connection", car.id);
                let old_driver_idx = db.drivers_by_id[&car.id];
                let old_driver = &mut db.drivers[old_driver_idx];
                trace!("Old driver info: ({:#?})", old_driver);

                if old_driver.guid != info.driver_guid {
                    warn!(
                        "Old driver ({}) is not the same as new driver ({})",
                        old_driver.guid, info.driver_guid
                    );
                    old_driver.car = None;
                    old_driver.is_connected = false;
                }

                db.drivers_by_id.remove(&car.id);
            }
            _ => {}
        }

        debug!("Adding driver to ID DB with ID ({})", info.car_id);
        db.drivers_by_id.insert(info.car_id, index);

        Ok(())
    }
}

#[derive(Debug)]
pub struct DriverDB {
    pub(crate) db: Rc<RefCell<DriverDBInternal>>,
}

impl DriverDB {
    pub fn get_driver_by_guid<'a>(&'a self, guid: &str) -> Option<DriverRef<'a>> {
        let db_ref = self.db.borrow();

        let id = *db_ref.drivers_by_guid.get(guid)?;

        Some(DriverRef { db_ref, id })
    }

    pub fn get_driver_by_id(&self, id: u8) -> Option<DriverRef> {
        let db_ref = self.db.borrow();

        let id = *db_ref.drivers_by_id.get(&id)?;
        Some(DriverRef { db_ref, id })
    }

    pub fn get_connected_drivers(&self) -> DriverCollectionRef {
        let db_ref = self.db.borrow();
        let drivers = db_ref.drivers_by_id.values().copied().collect();

        DriverCollectionRef { db_ref, drivers }
    }

    pub fn get_all_drivers(&self) -> DriverCollectionRef {
        let db_ref = self.db.borrow();
        let drivers = db_ref.drivers_by_guid.values().copied().collect();

        DriverCollectionRef { db_ref, drivers }
    }
}

#[derive(Debug)]
pub struct DriverRef<'a> {
    db_ref: Ref<'a, DriverDBInternal>,
    id: usize,
}

impl<'a> Deref for DriverRef<'a> {
    type Target = Driver;

    fn deref(&self) -> &Self::Target {
        &self.db_ref.drivers[self.id]
    }
}
#[derive(Debug)]
pub struct DriverCollectionRef<'a> {
    db_ref: Ref<'a, DriverDBInternal>,
    drivers: Vec<usize>,
}

impl<'a> DriverCollectionRef<'a> {
    pub fn iter(&'a self) -> impl Iterator<Item = &'a Driver> {
        self.drivers.iter().map(move |&id| &self.db_ref.drivers[id])
    }
}

impl<'a> Deref for DriverCollectionRef<'a> {
    type Target = [usize];

    fn deref(&self) -> &Self::Target {
        &self.drivers
    }
}
