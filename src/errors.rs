#[derive(Debug, Fail)]
pub enum ErrorKind {
    #[fail(display = "Unable to bind to socket")]
    SocketBindError,

    #[fail(display = "Unable to receive data")]
    RecieveError,
}