use ::std::net::SocketAddrV4;

#[derive(Debug, Copy, Clone)]
pub struct ServerPoint {
    pub command_port: SocketAddrV4,
    pub data_port: SocketAddrV4,
}

#[derive(Debug, Copy, Clone)]
pub struct Config {
    pub server: ServerPoint,
    pub forward: Option<ServerPoint>,
}