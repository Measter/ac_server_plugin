use crate::{
    commander::Commander,
    driver_db::{DriverDB, DriverDBHandler, DriverDBInternal},
    error::ServerError,
    handler::EventHandler,
    protocol::{AcsMessage, AcsMessageId},
};

use std::{
    cell::RefCell,
    convert::TryInto,
    io,
    net::{SocketAddr, UdpSocket},
    rc::Rc,
};

use log::{debug, error, info, trace};

#[derive(Debug, Clone)]
pub struct PluginPoint {
    pub command_port: SocketAddr,
    pub data_port: SocketAddr,
}

#[derive(Debug, Clone)]
pub struct ServerConfig {
    pub server: PluginPoint,
    pub forward: Option<PluginPoint>,
}

pub struct Server {
    config: ServerConfig,
    handlers: Vec<Box<dyn EventHandler>>,
    is_known_session: bool,
}

impl Server {
    pub fn new(config: ServerConfig) -> Self {
        Self {
            config,
            handlers: Vec::new(),
            is_known_session: false,
        }
    }

    pub fn get_driver_db(&mut self) -> DriverDB {
        let driver_db_internal = DriverDBInternal::default();
        let driver_db_internal = Rc::new(RefCell::new(driver_db_internal));
        let handler = DriverDBHandler {
            db: Rc::clone(&driver_db_internal),
        };

        self.add_event_handler(handler);

        DriverDB {
            db: driver_db_internal,
        }
    }

    pub fn add_event_handler(&mut self, handler: impl EventHandler + 'static) {
        debug!("Adding event handler: {}", handler.name());
        self.handlers.push(Box::new(handler));
    }

    pub fn run(&mut self) -> Result<(), ServerError> {
        info!("Running server plugin:");
        info!("Server Data Port: {}", self.config.server.data_port);
        info!("Server Command Port: {}", self.config.server.command_port);

        if let Some(fwd) = &self.config.forward {
            info!("Forward Data Port: {}", fwd.data_port);
            info!("Forward Command Port: {}", fwd.command_port);
        }

        info!("Opening UDP client at {}", self.config.server.data_port);
        let server_client = UdpSocket::bind(self.config.server.data_port)?;
        server_client.connect(self.config.server.command_port)?;

        // This is a little awkward to do in one operation due to moving the client.
        // So, we'll just create the client here, and spawn the thread later.
        // TODO: Sort out forwarding fuckery with the commander.
        let forward_client = if let Some(fwd) = &self.config.forward {
            debug!(
                "Opening forwarding client from {} to {}",
                fwd.command_port, self.config.server.command_port
            );
            let client = UdpSocket::bind(fwd.command_port)?;
            client.connect(fwd.data_port)?;
            Some(client)
        } else {
            None
        };

        crossbeam_utils::thread::scope(|s| -> Result<(), ServerError> {
            let _fwd_thread =
                s.spawn(|_| Server::forward_thread(&server_client, forward_client.as_ref()));

            debug!("Creating Commander");
            let mut commander = Commander {
                server: &server_client,
                buffer: Vec::with_capacity(0x100),
            };

            debug!("Waiting for messages from server");
            let mut buffer = vec![0; 0x1_0000];

            loop {
                let len = server_client.recv(&mut buffer)?;
                debug!("Recieved data packet.");
                let buffer = &buffer[..len];

                if let Some(fwd) = &forward_client {
                    debug!("Forwarding data packet");
                    fwd.send(&buffer)?;
                }

                let packet_id: AcsMessageId = match buffer[0].try_into() {
                    Ok(p) => p,
                    Err(e) => {
                        error!("Invalid packet ID: {}", e.0);
                        continue;
                    }
                };
                debug!("Packet type: {:?}", packet_id);

                if !self.is_known_session
                    && !matches!(
                        packet_id,
                        AcsMessageId::Version
                            | AcsMessageId::SessionInfo
                            | AcsMessageId::NewSession
                    )
                {
                    info!("Unknown session, requesting info.");
                    if let Err(e) = commander.get_session_info_current() {
                        error!("Error ocurred while requesting current session info: {}", e);
                    }
                }

                let packet = match crate::protocol::parse_message(&buffer[1..], packet_id) {
                    Ok((_, p)) => p,
                    Err(err) => {
                        error!("Error parsing packet: {:?}", err);
                        continue;
                    }
                };

                trace!("Packet contents: {:#?}", packet);

                let res = match packet {
                    AcsMessage::Version(i) => self
                        .handlers
                        .iter_mut()
                        .try_for_each(|f| f.on_protocol_version(&mut commander, i)),
                    AcsMessage::NewSession(i) => {
                        let res = self
                            .handlers
                            .iter_mut()
                            .try_for_each(|f| f.on_new_session(&mut commander, &i));
                        self.is_known_session = true;
                        res
                    }
                    AcsMessage::NewConnection(i) => self
                        .handlers
                        .iter_mut()
                        .try_for_each(|f| f.on_new_connection(&mut commander, &i)),
                    AcsMessage::ConnectionClosed(i) => self
                        .handlers
                        .iter_mut()
                        .try_for_each(|f| f.on_connection_closed(&mut commander, &i)),
                    AcsMessage::ClientLoaded(i) => self
                        .handlers
                        .iter_mut()
                        .try_for_each(|f| f.on_client_loaded(&mut commander, i)),
                    AcsMessage::SessionInfo(i) => {
                        let res = self
                            .handlers
                            .iter_mut()
                            .try_for_each(|f| f.on_session_info(&mut commander, &i));
                        self.is_known_session = true;
                        res
                    }
                    AcsMessage::Chat(i) => self
                        .handlers
                        .iter_mut()
                        .try_for_each(|f| f.on_chat_message(&mut commander, &i)),
                    AcsMessage::CarUpdate(i) => self
                        .handlers
                        .iter_mut()
                        .try_for_each(|f| f.on_car_update(&mut commander, &i)),
                    AcsMessage::CarInfo(i) => self
                        .handlers
                        .iter_mut()
                        .try_for_each(|f| f.on_car_info(&mut commander, &i)),
                    AcsMessage::EndSession(i) => self
                        .handlers
                        .iter_mut()
                        .try_for_each(|f| f.on_end_session(&mut commander, &i)),
                    AcsMessage::Error(i) => self
                        .handlers
                        .iter_mut()
                        .try_for_each(|f| f.on_error(&mut commander, &i)),
                    AcsMessage::LapCompleted(i) => self
                        .handlers
                        .iter_mut()
                        .try_for_each(|f| f.on_lap_completed(&mut commander, &i)),
                    AcsMessage::ClientEvent(i) => self
                        .handlers
                        .iter_mut()
                        .try_for_each(|f| f.on_client_event(&mut commander, &i)),
                };

                if let Err(e) = res {
                    error!("Error ocurred during handler envokation: {}", e);
                }
            }
        })
        .map_err(|e| ServerError::UnhandledResult(e))??;

        Ok(())
    }

    fn forward_thread(server: &UdpSocket, forward_client: Option<&UdpSocket>) {
        let forward_client = if let Some(cl) = forward_client {
            cl
        } else {
            return;
        };

        info!("Started forwarding thread");
        let mut buffer = vec![0; 0x1_0000];

        loop {
            let res = (|| -> Result<(), io::Error> {
                debug!("Awaiting message to forward");
                let len = forward_client.recv(&mut buffer)?;
                let buffer = &buffer[..len];

                trace!("Forward packet with ID: {}", buffer[0]);
                server.send(&buffer)?;
                debug!("Message forwarded");
                Ok(())
            })();

            match res {
                Err(e) if e.raw_os_error() == Some(10054) => {
                    error!("Forwarding connection was closed by remote host")
                }
                Err(e) => error!("An error ocurred while forwarding: {}", e),
                Ok(_) => {}
            }
        }
    }
}
