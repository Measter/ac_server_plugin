use crate::{
    protocol::{AcsCommand, SessionType, SessionLength},
    error::ServerError,
};

use byteorder::{LittleEndian, WriteBytesExt};
use log::*;

use std::{
    net::UdpSocket,
    time::Duration,
};

pub struct Commander<'a> {
    pub(crate) server: &'a UdpSocket,
    pub(crate) buffer: Vec<u8>,
}

impl<'a> Commander<'a> {
    /// Note: Appends to existing buffer.
    fn write_unicode_string(&mut self, msg: &str) {
        trace!("Writing UTF32 string");
        trace!("String contents: {:?}", msg);
        let char_len = msg.chars().count();

        // Unwrapping here because if it fails to write to memory, then things are really bad.
        self.buffer.write_u8(char_len as u8).unwrap();
        for c in msg.chars() {
            self.buffer.write_u32::<LittleEndian>(c as u32).unwrap();
        }
    }

    pub fn send_chat(&mut self, car_id: u8, msg: &str) -> Result<(), ServerError> {
        debug!("Sending chat message to car {}", car_id);
        trace!("Message contents: {}", msg);

        self.buffer.clear();
        // Unwrapping here because faling here means we failed to write to memory.
        self.buffer.write_u8(AcsCommand::SendChat as u8).unwrap();
        self.buffer.write_u8(car_id).unwrap();
        self.write_unicode_string(msg);

        self.server.send(&self.buffer)?;

        Ok(())
    }

    pub fn set_realtime_interval(&mut self, interval: Duration) -> Result<(), ServerError> {
        debug!("Setting realtime position interval to {}ms", interval.as_millis());

        self.buffer.clear();
        // Unwrapping because we're writing to memory.
        self.buffer.write_u8(AcsCommand::RealtimePosInterval as u8).unwrap();
        self.buffer.write_u16::<LittleEndian>(interval.as_millis() as u16).unwrap();

        self.server.send(&self.buffer)?;
        Ok(())
    }

    pub fn get_car_info(&mut self, car_id: u8) -> Result<(), ServerError> {
        debug!("Car info request for car {}", car_id);

        self.buffer.clear();
        // Unwrapping because we're writing to memory.
        self.buffer.write_u8(AcsCommand::GetCarInfo as u8).unwrap();
        self.buffer.write_u8(car_id).unwrap();

        self.server.send(&self.buffer)?;
        Ok(())
    }

    pub fn get_session_info_current(&mut self) -> Result<(), ServerError> {
        debug!("Current session info request");

        self.buffer.clear();
        // Unwrapping because we're writing to memory.
        self.buffer.write_u8(AcsCommand::GetSessionInfo as u8).unwrap();
        self.buffer.write_i16::<LittleEndian>(-1).unwrap();

        self.server.send(&self.buffer)?;
        Ok(())
    }

    pub fn get_session_info(&mut self, session_id: u8) -> Result<(), ServerError> {
        debug!("Session info request for session {}", session_id);

        self.buffer.clear();
        // Unwrapping because we're writing to memory.
        self.buffer.write_u8(AcsCommand::GetSessionInfo as u8).unwrap();
        self.buffer.write_i16::<LittleEndian>(session_id as i16).unwrap();

        self.server.send(&self.buffer)?;
        Ok(())
    }

    pub fn set_session_info(
        &mut self,
        session_id: u8,
        name: &str,
        session_type: SessionType,
        length: SessionLength,
        wait_time: u32
    ) -> Result<(), ServerError> {
        debug!("Setting session info for session {}", session_id);
        trace!("\tName: {}", name);
        trace!("\tType: {:?}", session_type);
        trace!("\tLength: {:?}", length);
        trace!("\tWait Time: {}", wait_time);

        self.buffer.clear();
        // Plugin example says we need to trim the name to 20 characters.
        let byte_end: usize = name.char_indices()
            .take(20)
            .last()
            .map(|(idx, c)| idx + c.len_utf8())
            .ok_or(ServerError::EmptyNameError)?;

        let trimmed_name = &name[..byte_end];

        // Unwrapping because we're writing to memory.
        self.buffer.write_u8(AcsCommand::SetSessionInfo as u8).unwrap();
        self.buffer.write_u8(session_id).unwrap();
        self.write_unicode_string(trimmed_name);
        self.buffer.write_u8(session_type as u8).unwrap();
        
        let (len_laps, len_sec) = match length {
            SessionLength::Laps(laps) => (laps as u32, 0 as u32),
            SessionLength::Time(time) => (0, time.as_secs() as u32),
        };

        self.buffer.write_u32::<LittleEndian>(len_laps).unwrap();
        self.buffer.write_u32::<LittleEndian>(len_sec).unwrap();
        self.buffer.write_u32::<LittleEndian>(wait_time).unwrap();

        self.server.send(&self.buffer)?;

        Ok(())
    }

    pub fn kick_driver(&mut self, car_id: u8) -> Result<(), ServerError> {
        debug!("Kick driver request for car {}", car_id);

        self.buffer.clear();
        // Unwrap because we're writing to memory.
        self.buffer.write_u8(AcsCommand::KickUser as u8).unwrap();
        self.buffer.write_u8(car_id).unwrap();

        self.server.send(&self.buffer)?;

        Ok(())
    }

    pub fn next_sessoin(&mut self) -> Result<(), ServerError> {
        debug!("Next session request");

        self.buffer.clear();
        // Unwrap because we're writing to memory.
        self.buffer.write_u8(AcsCommand::NextSession as u8).unwrap();

        self.server.send(&self.buffer)?;
        Ok(())
    }

    pub fn restart_session(&mut self) -> Result<(), ServerError> {
        debug!("Restart session request");
        
        self.buffer.clear();

        // Unwrap because we're writing to memory.
        self.buffer.write_u8(AcsCommand::RestartSession as u8).unwrap();
        self.server.send(&self.buffer)?;

        Ok(())
    }

    pub fn broadcast_message(&mut self, msg: &str) -> Result<(), ServerError> {
        debug!("Sending broadcast message");
        trace!("Message contents: {}", msg);

        self.buffer.clear();
        // Unwrap because we're writing to memory.
        self.buffer.write_u8(AcsCommand::BroadcastChat as u8).unwrap();
        self.write_unicode_string(msg);

        self.server.send(&self.buffer)?;

        Ok(())
    }

    pub fn set_ballast(&mut self, car_id: u8, amount: u8) -> Result<(), ServerError> {
        debug!("Set ballast to {} for car {}", amount, car_id);
        if amount > 150 {
            return Err(ServerError::InvalidBallastAmount(amount));
        }

        let msg = format!("/ballast {} {}", car_id, amount);
        self.send_admin_command(&msg)?;

        Ok(())
    }

    pub fn set_restrictor(&mut self, car_id: u8, amount: u8) -> Result<(), ServerError> {
        debug!("Set restrictor to {} for car {}", amount, car_id);
        if amount > 100 {
            return Err(ServerError::InvalidRestrictorAmount(amount));
        }

        let msg = format!("/restrictor {} {}", car_id, amount);
        self.send_admin_command(&msg)?;

        Ok(())
    }
    pub fn ban_driver(&mut self, car_id: u8) -> Result<(), ServerError> {
        debug!("Ban driver request for car {}", car_id);

        let msg = format!("/ban_id {}", car_id);
        self.send_admin_command(&msg)?;

        Ok(())
    }
    
    pub fn send_admin_command(&mut self, cmd: &str) -> Result<(), ServerError> {
        debug!("Admin command request");
        trace!("Command contents: {}", cmd);

        self.buffer.clear();
        // Unwrap because we're writing to memory.
        self.buffer.write_u8(AcsCommand::AdminCommand as u8).unwrap();
        self.write_unicode_string(cmd);

        self.server.send(&self.buffer)?;

        Ok(())
    }
}